
package evolaze.java;

import static evolaze.java.Controller.DefaultController.defaultAction;

import static spark.Spark.get;

public class Main {
    public static void main(String[] args) {
        get("/", (request, response) -> defaultAction(request, response));
    }
}
