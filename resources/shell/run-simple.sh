#!/usr/bin/env bash

. ${PWD}/resources/env/main.env

docker build \
    --build-arg="APP_NAME=${APP_NAME}" \
    --build-arg="APP_PORT=${APP_PORT}" \
    --build-arg="APP_DESCRIPTOR_REF=${APP_DESCRIPTOR_REF}" \
    --build-arg="APP_MAIN_CLASS=${APP_MAIN_CLASS}" \
    --build-arg="APP_PATH=./app" \
    -t ${APP_NAME}:${APP_VERSION} \
    -f ${PWD}/docker/simple.Dockerfile . \
&& docker run -d -p ${APP_PORT}:${APP_PORT} --name ${APP_NAME} ${APP_NAME}:${APP_VERSION}
