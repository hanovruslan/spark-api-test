FROM java:8
# Install maven
RUN apt-get update
RUN apt-get install -y maven
ARG APP_PATH
ARG APP_PORT
ARG APP_NAME
ARG APP_DESCRIPTOR_REF
ARG APP_MAIN_CLASS
ARG JDK_PATH=/usr/lib/jvm/java-8-openjdk-amd64/bin/java
ENV APP_PATH=${APP_PATH} \
    APP_NAME=${APP_NAME} \
    APP_DESCRIPTOR_REF=${APP_DESCRIPTOR_REF} \
    APP_MAIN_CLASS=${APP_MAIN_CLASS} \
    JDK_PATH=${JDK_PATH}
WORKDIR /code
# Prepare by downloading dependencies
ADD ${APP_PATH}/pom.xml /code/pom.xml
RUN mvn dependency:resolve \
        -Dapp.name=${APP_NAME} \
        -Dapp.descriptor.ref=${APP_DESCRIPTOR_REF} \
        -Dapp.main.class=${APP_MAIN_CLASS} \
    && mvn verify
# Adding source, compile and package into a fat jar
ADD ${APP_PATH}/src /code/src
RUN mvn package
EXPOSE ${APP_PORT}
CMD ${JDK_PATH} -jar target/${APP_NAME}-${APP_DESCRIPTOR_REF}.jar
